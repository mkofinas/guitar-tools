import random
import time

guitar_notes = {
    6: ['E', 'F', 'Gb', 'G', 'Ab', 'A', 'Bb', 'B', 'C', 'Db', 'D', 'Eb', 'E'],
    5: ['A', 'Bb', 'B', 'C', 'Db', 'D', 'Eb', 'E', 'F', 'Gb', 'G', 'Ab', 'A']
}

game_duration = 300
start_time = time.time()
while True:
    random_string = random.randint(5, 6)
    random_fret = random.randint(0, 12)
 
    pred = input(f'{random_string}th string, {random_fret}th fret: ')
    if pred == guitar_notes[random_string][random_fret]:
        print('Correct')
    else:
        print(f'Wrong! {guitar_notes[random_string][random_fret]}')
    if time.time() - start_time > game_duration:
        break
