#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from builtins import range

import numpy as np


def next_note(scale, note):
    idx = scale.index(note)
    next_idx = idx + 1 if idx + 1 < len(scale) else 0
    return scale[next_idx]


chromatic_scale = [u'A', u'A♯', u'B', u'C', u'C♯', u'D', u'D♯', u'E', u'F',
                   u'F♯', u'G', u'G♯']

guitar_tuning = [u'E', u'B', u'G', u'D', u'A', u'E']
tuning_octaves = [2, 3, 3, 3, 4, 4]

num_frets = 24

fretboard_notes = np.chararray((len(guitar_tuning), num_frets+1), itemsize=2,
                               unicode=True)

fretboard_notes[:, 0] = guitar_tuning

for i in range(1, num_frets+1):
    fretboard_notes[:, i] = [next_note(chromatic_scale, n) for n in fretboard_notes[:, i-1]]

print('   ', end='')
for i in range(num_frets+1):
    print('   ' + str(i), end='') if len(str(i)) == 1 else print('  ' + str(i), end='')
print()

topsplitline = ('┎' + 3 * '─' + '┰'
                + ''.join((num_frets-1) * [3 * '─' + '┬'])
                + 3 * '─' + '┒')
splitline = ('┠' + 3 * '─' + '╂'
             + ''.join((num_frets-1) * [3 * '─' + '┼'])
             + 3 * '─' + '┨')
bottomsplitline = ('┖' + 3 * '─' + '┸'
                   + ''.join((num_frets-1) * [3 * '─' + '┴'])
                   + 3 * '─' + '┚')
print('  ' + topsplitline)

for i, r in enumerate(fretboard_notes):
    print(str(i + 1), '┃', end='')
    for j, c in enumerate(r):
        end_mark = '┃' if j == 0 or j == num_frets else '│'
        print('  ' + c, end=end_mark) if len(c) == 1 else print(' ' + c, end=end_mark)
    print()
    if i < len(guitar_tuning) - 1:
        print('  ' + splitline)

print('  ' + bottomsplitline)
