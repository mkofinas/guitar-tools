from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import re

import numpy as np

chromatic_scale = ['A', 'A#', 'B', 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G',
                   'G#']

enharmonic_equivalents = {'Bb': 'A#', 'Db': 'C#', 'Eb': 'D#', 'Gb': 'F#',
                          'Ab': 'G#'}

naming_changes = {'A': 'La', 'A#': 'La#', 'B': 'Si', 'C': 'Do', 'C#': 'Do#',
                  'D': 'Re', 'D#': 'Re#', 'E': 'Mi', 'F': 'Fa',
                  'F#': 'Fa#', 'G': 'Sol', 'G#': 'Sol#'}

def translate_note(note, num_semitones):
    idx = chromatic_scale.index(note)
    next_idx = (idx + num_semitones) % len(chromatic_scale)
    return chromatic_scale[next_idx]

with open('o_akrovatis.tab', 'r') as f:
    x = [l.rstrip('\n') for l in f.readlines()]
    if x[-1]:
        x.append('')

for l in x:
    print(l)

same_tab = False
translated_song = []
translated_tab = []
for l in x:
    m = re.match('([A-Ga-g][b#]?)([/|])(.*)', l)
    if m:
        base_note = m.group(1).capitalize()
        base_note = (enharmonic_equivalents[base_note]
                     if base_note in enharmonic_equivalents
                     else base_note)

        translated_notes = [translate_note(base_note, int(note))
                            if note.isdigit() else note
                            for note in m.group(3)]
        translated_notes.insert(0, m.group(2))
        translated_notes.insert(0, base_note)

        translated_tab.append(translated_notes)
        same_tab = True
    else:
        if translated_tab:
            fretboard_notes = np.chararray((len(translated_tab),
                                            len(translated_tab[0])),
                                           itemsize=2)
            for i, t in enumerate(translated_tab):
                fretboard_notes[i] = t
            translated_song.append(fretboard_notes)
            translated_tab = []
        same_tab = False

new_translated = []
for i, part in enumerate(translated_song):
    new_translated_row = []
    for j in range(2, len(part[0])):
        col = part[:, j]
        new_col = [naming_changes[item] for item in col
                   if item in naming_changes]
        if len(new_col) == 1:
            new_translated_row.extend(new_col)
        elif len(new_col) == 2:
            new_translated_row.append(new_col)
    if new_translated_row:
        new_translated.append(new_translated_row)

for i, part in enumerate(translated_song):
    for j in range(2, len(part[0])):
        col = part[:, j]
        if max([len(item) for item in col]) == 2:
            new_col = [' ' + item if len(item) == 1 else item for item in col]
            translated_song[i][:, j] = new_col

for part in translated_song:
    for i in range(len(part)):
        print(''.join(part[i]))
    print()

for r in new_translated:
    for it in r:
        print(it, end=' ')
    print()
