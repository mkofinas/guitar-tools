# Guitar Tools

## Scale Boxes

![Scale Boxes](images/scales.png)

## Guitar Tablature Library

| Symbol | Technique
| ------ | --------------
| h      | hammer on
| p      | pull off
| b      | bend string up
| r      | release bend
| /      | slide up
| \      | slide down

_All theory components have been moved to https://gitlab.com/mkofinas/music-theory._
